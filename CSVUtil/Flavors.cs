﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSVUtil
{
    class Flavors
    {
        public static void StartProgram()
        {
            string myFileContents;

            try
            {
                // Read text data and store it in a string.
                myFileContents = File.ReadAllText(@"C:\Users\Public\flavors.csv");
            }
            catch (Exception exc)
            {
                // Fill with custom data on first run.
                myFileContents = "First,FavoriteFlavor";
            }

            // Create a new CSVFile Object to handle our data.
            CSVFile csv = new CSVFile(myFileContents);

            ArrayList fields = csv.FieldsAsArray();

            string firstName = AskForFirstName();

            // Check if this person has already told us their favorite flavor.
            int previousFlavorIndex = RowThatMatches("First", firstName, fields);
            if (previousFlavorIndex != -1)
            {
                // Retrieve the flavor from the stored data.
                string previousFlavor = (string)((ArrayList)fields[previousFlavorIndex])[IndexOfColumn("FavoriteFlavor", fields)];
                Console.WriteLine("Let me guess... Your favorite ice cream flavor is {0}!", previousFlavor);

                // Exit the program
                return;
            }

            string favFlavor = AskForFlavor();

            // Tell the user we got their input.
            Console.WriteLine("Got it! {0} is one of my favorites too!", favFlavor);

            // Insert the new data
            fields.Add(new ArrayList { firstName, favFlavor });

            // Convert the fields ArrayList back into a CSV string:
            string newCSVFileOutput = CSVFile.CSVStringFromArray(fields);

            // Write the new generated CSV to a file.
            File.WriteAllText(@"C:\Users\Public\flavors.csv", newCSVFileOutput);
        }

        public static string AskForFirstName()
        {
            // Prompt the user for their first name
            Console.WriteLine("Please enter your first name: ");

            string firstName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(firstName))
            {
                Console.WriteLine("Please do not leave this blank.");
                Console.WriteLine("Please enter your first name: ");
                firstName = Console.ReadLine();
            }

            return firstName;
        }

        public static string AskForFlavor()
        {
            Console.WriteLine("What is your favorite flavor of ice cream: ");

            string favFlavor = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(favFlavor))
            {
                Console.WriteLine("Please do not leave this blank.");
                Console.WriteLine("What is your favorite flavor of ice cream: ");
                favFlavor = Console.ReadLine();
            }

            return favFlavor;
        }

        // This method returns the index of the column with the name `columnName`.
        public static int IndexOfColumn(string columnName, ArrayList fields)
        {

            ArrayList firstRow = (ArrayList)fields[0];
            return firstRow.IndexOf(columnName);
        }

        // This method searches all rows in a column and returns the row that matches first.
        public static int RowThatMatches(string columnName, string matchText, ArrayList fields)
        {
            int colIndex = IndexOfColumn(columnName, fields);
            // Column not found
            if (colIndex == -1) return -1;

            // Loop through each row and compare matchText to the column's text.
            for (int i = 0; i < fields.Count; i++)
            {
                ArrayList row = (ArrayList)fields[i];

                string textFoundInCol = (string)row[colIndex];
                // Check if we have found what we are looking for:
                if (textFoundInCol.ToLower() == matchText.ToLower())
                {
                    return i;
                }
            }

            // Default to not found
            return -1;
        }

    }
}
