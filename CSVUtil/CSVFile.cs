﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVUtil
{
    // Name: Bailey Seymour
    // Date: 1803
    // Course: Project & Portfolio 1
    // Synopsis: This program parses C,S,V files.

    class CSVFile
    {
        string _fileContents;
        public CSVFile(string fileContents)
        {
            _fileContents = fileContents;
        }

        public ArrayList FieldsAsArray()
        {
            ArrayList parsedList = new ArrayList();

            // Split each line in the file at the newline char
            string[] lines = _fileContents.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                // Split each line into parts at each comma.
                string[] commaSeparatedLine = line.Split(',');

                // Create a new ArrayList to store the values.
                ArrayList lineArrayList = new ArrayList();

                for (int j = 0; j < commaSeparatedLine.Length; j++)
                {
                    lineArrayList.Add(commaSeparatedLine[j]);
                }

                if (lineArrayList.Count > 0)
                {
                    parsedList.Add(lineArrayList);
                }
            }

            return parsedList;
        }

        public static string CSVStringFromArray(ArrayList fields)
        {
            string csvString = "";

            for (int i = 0; i < fields.Count; i++)
            {
                ArrayList lineArray = (ArrayList)fields[i];
                
                // For each line add the items as a comma separated list.
                for (int j = 0; j < lineArray.Count; j++)
                {
                    string listItem = (string)lineArray[j];
                    if (j + 1 == lineArray.Count)
                        csvString += listItem;
                    else
                        csvString += listItem + ",";
                }

                // Append a newline after each line is written.
                csvString += Environment.NewLine;
            }

            return csvString;
        }

    }
}
